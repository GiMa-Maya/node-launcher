#!/bin/sh

# trunk-ignore(shellcheck/SC3040)
set -euo pipefail

REGISTRY="registry.gitlab.com/mayachain/devops/node-launcher"

docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

find ci/images/ -name version -printf '%h\n' | xargs basename -a | while read -r image; do
  version=$(cat "ci/images/$image/version")

  # check to see if image version is already published
  if docker manifest inspect "$REGISTRY:${image}-${version}" >/dev/null 2>&1; then
    echo "Image ${image}:${version} already published."
  else
    echo "Building image $image:$version..."
    docker build -t "$REGISTRY:$image-$version" "ci/images/$image"
    if [ "$CI_COMMIT_BRANCH" = "master" ]; then
      docker push "$REGISTRY:$image-$version"
    fi
  fi

  # For TC also build the mocknet image
  if [ "$image" = "thornode-daemon" ]; then
    if docker manifest inspect "$REGISTRY:${image}-mocknet-${version}" >/dev/null 2>&1; then
      echo "Image ${image}:${version} (mocknet) already published."
    else
      echo "Building image $image:$version..."
      docker build -t "$REGISTRY:${image}-mocknet-${version}" -f ci/images/thornode-daemon/Dockerfile.mocknet "ci/images/$image"
      if [ "$CI_COMMIT_BRANCH" = "master" ]; then
        docker push "$REGISTRY:${image}-mocknet-${version}"
      fi
    fi
  fi
done
