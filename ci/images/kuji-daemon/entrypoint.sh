#!/bin/bash

RPC_PORT=${RPC_PORT:-26657}
GRPC_PORT=${GRPC_PORT:-9090}

if [[ ! -f "/root/.kujira/config/app.toml" ]]; then
  cp /etc/kuji/app.toml /root/.kujira/config/app.toml
fi

exec /kujirad start --log_format json --rpc.laddr "tcp://0.0.0.0:$RPC_PORT" --grpc.address "0.0.0.0:$GRPC_PORT" --x-crisis-skip-assert-invariants "$@"
