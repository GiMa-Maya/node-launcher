#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Setting MAYANode keys"
kubectl exec -it -n "$NAME" -c mayanode deploy/mayanode -- /kube-scripts/set-node-keys.sh
sleep 5
echo MAYANode Keys updated
