#!/usr/bin/env bash

set -e

source ./scripts/core.sh

echo "Maya only provides mainnet snapshots. Continue?"
confirm
NET="mainnet"

get_node_info_short

if ! node_exists; then
  die "No existing mayanode found, make sure this is the correct name"
fi

echo "=> Select snapshot type"
menu pruned pruned full
PREFIX=$MENU_SELECTED
HEIGHTS=$(curl -s https://public-snapshots-mayanode.s3.amazonaws.com/\?delimiter=%2F\&prefix="${PREFIX}"/ | grep -oP "(?<=<Prefix>${PREFIX}/)\d+")
echo "=> Select snapshot height"
# trunk-ignore(shellcheck/SC2046,shellcheck/SC2086): Intentional word splitting
menu $(echo $HEIGHTS | awk '{print $NF}') $HEIGHTS
HEIGHT=$MENU_SELECTED

# stop mayanode
echo "stopping mayanode..."
kubectl scale -n "$NAME" --replicas=0 deploy/mayanode --timeout=5m
kubectl wait --for=delete pods -l app.kubernetes.io/name=mayanode -n "$NAME" --timeout=5m >/dev/null 2>&1 || true

# create recover pod
echo "creating recover pod"
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: recover-mayanode
  namespace: $NAME
spec:
  containers:
  - name: recover
    image: amazon/aws-cli:latest
    command:
      - tail
      - -f
      - /dev/null
    volumeMounts:
    - mountPath: /root
      name: data
  volumes:
  - name: data
    persistentVolumeClaim:
      claimName: mayanode
EOF

# reset node state
echo "waiting for recover pod to be ready..."
kubectl wait --for=condition=ready pods/recover-mayanode -n "$NAME" --timeout=5m >/dev/null 2>&1

# recover maya snapshot
echo "pulling maya snapshot..."
kubectl exec -n "$NAME" -it recover-mayanode -- aws s3 cp "s3://public-snapshots-mayanode/$PREFIX/$HEIGHT" /root/.mayanode/data/ --recursive --no-sign-request

echo "=> ${boldgreen}Proceeding to clean up recovery pod and restart mayanode$reset"
confirm

echo "cleaning up recover pod"
kubectl -n "$NAME" delete pod/recover-mayanode

# start mayanode
kubectl scale -n "$NAME" --replicas=1 deploy/mayanode --timeout=5m
