#!/usr/bin/env bash

source ./scripts/core.sh

get_node_info_short

echo "Performing snapshot and backup for MAYANode and Bifrost..."
confirm

if snapshot_available; then
  make_snapshot mayanode
  make_snapshot bifrost
else
  warn "Snapshot not available in this cluster, performing backup only..."
  echo
fi

make_backup mayanode
make_backup bifrost
