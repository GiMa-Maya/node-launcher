#!/usr/bin/env bash

source ./scripts/core.sh

get_node_info_short
echo "=> Select a MAYANode service to reset"
SERVICE=mayanode

if node_exists; then
  echo
  warn "Found an existing MAYANode, make sure this is the node you want to update:"
  display_status
  echo
fi

echo "=> Resetting service $boldyellow$SERVICE$reset of a MAYANode named $boldyellow$NAME$reset"
echo
warn "Destructive command, be careful, your service data volume data will be wiped out and restarted to sync from scratch"
confirm

IMAGE=$(kubectl -n "$NAME" get deploy/mayanode -o jsonpath='{$.spec.template.spec.containers[:1].image}')
SPEC="
{
  \"apiVersion\": \"v1\",
  \"spec\": {
    \"containers\": [
      {
        \"command\": [
          \"sh\",
          \"-c\",
          \"mayanode unsafe-reset-all\"
        ],
        \"name\": \"debug-mayanode\",
        \"stdin\": true,
        \"tty\": true,
        \"image\": \"$IMAGE\",
        \"volumeMounts\": [{\"mountPath\": \"/root\", \"name\":\"data\"}]
      }
    ],
    \"volumes\": [{\"name\": \"data\", \"persistentVolumeClaim\": {\"claimName\": \"mayanode\"}}]
  }
}"

kubectl scale -n "$NAME" --replicas=0 deploy/mayanode --timeout=5m
kubectl wait --for=delete pods -l app.kubernetes.io/name=mayanode -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
kubectl run -n "$NAME" -it reset-mayanode --rm --restart=Never --image="$IMAGE" --overrides="$SPEC"

kubectl scale -n "$NAME" --replicas=1 deploy/mayanode --timeout=5m
